## Getting started... with docker!

## What do I need to get started?

- a [POSIX compliant shell](https://pubs.opengroup.org/onlinepubs/9699919799/)
- [Docker Engine version 20.10.13 or greater](https://docs.docker.com/engine/)
(for docker compose plugin compatibility)
- [Docker Compose](https://docs.docker.com/compose/)
- [Add your user to the docker group](https://docs.docker.com/engine/install/linux-postinstall/)

## How do I start the tests?

```
git clone git@bitbucket.org:olivierdugas/formation-python.git
./scripts/init.sh
./scripts/test.sh
```

### The Short Version

The shortest path from sources to a build is as follow:

1. clone this project on your machine
  - `git clone git@bitbucket.org:olivierdugas/formation-python.git`
2. initialize the virtual development environment
  - execute user script [./scripts/init.sh](./scripts/init.sh)
3. build the project
  - execute user script [./scripts/build.sh](./scripts/build.sh)

Other actions you would wish to accomplish on the sources should have
their user script in the [scripts directory](./scripts). See
[User Scripts section](#user-scripts) section for a detailed list of
available user scripts in this project.

## User Scripts

_User Scripts_ are the scripts intended to be ran on the host machine to
accomplish the various actions a developer has to do in this project's
development workflow.

You can find the [User Scripts inside the scripts directory](./scripts/).

```
.
└── scripts
    ├── init.sh
    ├── shell.sh
    └── test.sh
```

### init.sh

Initialize the virtual development environment.

`init.sh` initializes a `./docker/user-setup/` folder to provide config files
to docker compose and builds the docker images required by docker compose. It
should be run when checking out the repo or when modifying a Dockerfile.

All dependencies and tools to compile the sources are installed,
configured and *archived* as a *Docker image*.

For a list of *actual* dependencies to build the sources
of this project, see [./docker/Dockerfile](./docker/Dockerfile).

### shell.sh

Get a shell session inside the virtual development environment.

### test.sh

Run all tests available in this project on most recent version of the
source available in this repository.

